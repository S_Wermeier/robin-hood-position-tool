# Robin Hood  MTool

Version 1.3.3


## Application

This Application is a Modding Tool for the Game Robin Hood and the Legends of Sherwood.

In this application, you can load game Maps to place units on them. The coordinates and unit types are stored in a database. The data is provided in PDF format. The goal is to utilize the created data for mapping new levels or missions.


## Install

1. Download the ZIP and extract the Files
2. Open the Application
3. The Restore Manager creates the required directories and data in few seconds.
4. Once the manager is complete, you can use the application


```
This Application is build on DotNet 6.0 Framework!
https://dotnet.microsoft.com/en-us/download/dotnet/6.0
```

## Function

- Load Maps
- Add / Delete Units (Creator)
- Switching the Units (currently 7 types) (Creator)
- Save / Load Unit Database (Creator)
- Print Unit List (PDF) or MAP (Create a PNG) 
- Show Unit Numbers and Boxes 
- Move Units (Drag and Drop) 
- Viewer Load MAP
- Show Over the Viewer all Soldiers and Civilians
- Restore Manager (Restore the Application Datebase)
- * Use the new Editor to Change RHM-Files! *
- Help Center


## Screenshots

Example
MAP : https://ibb.co/6ZQrXjj


## Virustotal

https://www.virustotal.com/gui/file/3d4a8992cf91ef739511ad2bb1f2464df7cc0165bad34b5bc380ddcdc7af1716/


## Support

You find Bug or have other Problems?
Please write me on Discord (ExiLe#8563)!

## Content

All images and graphics are from the Robin Hood Community.


## Project status

Version 1.3.X in Work.
